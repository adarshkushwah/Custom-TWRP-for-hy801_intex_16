# Custom-recovery-for-Intex-Cloud-M6-8gb-and-16gb
The repository consists of the custom recovery image for the Intex Cloud M6 8gb and 16gb smartphones ported and designed by me.

# Follow the instructions:-
1. Download the Minimal adb and fastboot tool.
2. Download this recovery image and paste it inside the Minimal folder's location.
3. Connect your device and make sure the usb debugging is on.
4. Type the following commands in the sequence:-
   
   1. <CODE>adb reboot bootloader</CODE>
   2. <CODE>fastboot flash recovery twrp.img</CODE>
   3. <CODE>fastboot reboot</CODE>
   
   The new recovery has been flashed. Open it by typing the following inside the Minimal's prompt:-
   
   <CODE>
   adb reboot recovery
   </CODE>
